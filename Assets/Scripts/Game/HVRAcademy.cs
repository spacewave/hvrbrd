﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MLAgents;

public class HVRAcademy : Academy {
    
    
    public override void AcademyReset()
    {
        List<GameObject> gameObjects = GameObject.FindGameObjectsWithTag("spawner").ToList();
        gameObjects.ForEach(o =>
        {
            RaceTrackPlayerSpawner raceTrackStartSpawner = o.GetComponent<RaceTrackPlayerSpawner>();
            if (raceTrackStartSpawner.willSpawnPlayers)
            {
                raceTrackStartSpawner.DespawnRacers();
            }
            raceTrackStartSpawner.SpawnRacers();
        });
    }

    public override void AcademyStep()
    {


    }

}
