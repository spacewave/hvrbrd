﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;

public class RaceController : MonoBehaviour
{
    public static RaceController Instance { get; private set; }

    public Race race;
    public List<Racer> racers;
    public int racerToDisplay = 0;
    
    public bool displayCanvas = true;
    public Canvas canvas;
    
    private Text jumbotronTextComponent;
    private Text roundTextComponent;
    

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        roundTextComponent = canvas.transform.GetChild(0).GetComponent<Text>();
        jumbotronTextComponent = canvas.transform.GetChild(2).GetComponent<Text>();
    } 

    public void UpdateRaceTrackNumber(int currentLoop)
    {
        if (displayCanvas)
        {
            race.currectLoop = currentLoop;
            roundTextComponent.text = "Round: " + currentLoop;
        }
    }

    public void IncrementLoopNumber(HVRAgent hvrAgent)
    {
        if (hvrAgent.racer.LoopCount == race.loopsPerGame)
        {
            if (hvrAgent.racer.racerType == RacerType.Player)
            {
                GameController.Instance.endGame = true;
                hvrAgent.racer.isFinished = true;
                racers.ForEach(racer =>
                {
                    if (racer.name.Equals(hvrAgent.racer.name))
                    {
                        racer.isFinished = true;
                    }
                });
                if (racers.TrueForAll(racer => racer.isFinished))
                {
                    Debug.Log("All reached goal");
                }
            }
        }
        else
        {
            hvrAgent.racer.IncrementLoopCount();
        }
    }

    private void finishRace()
    {
        ScoreBoard scoreBoard = ScoreBoardUtil.fetchScoreBoard();
        UnityTimeStopwatch unityTimeStopwatch = GameController.Instance.globalStopWatch;
        scoreBoard.scoreEntries.Add(new ScoreEntry(racers[racerToDisplay].name, unityTimeStopwatch.hours,
            unityTimeStopwatch.minutes, (int) unityTimeStopwatch.seconds, (int) unityTimeStopwatch.milliseconds));
        scoreBoard.scoreEntries.Sort();
        ScoreBoardUtil.ExportScoreBoard(scoreBoard);
        jumbotronTextComponent.text = "Finished Race";
    }

    public Racer IncrementLoopNumberOnRacer(Racer playerRacer)
    {
        if (playerRacer.LoopCount == race.loopsPerGame)
        {
            if (playerRacer.racerType == RacerType.Player)
            {
                GameController.Instance.endGame = true;
                playerRacer.isFinished = true;
                finishRace();
                racers.ForEach(racer =>
                {
                    if (racer.name.Equals(playerRacer.name))
                    {
                        racer.isFinished = true;
                    }
                });
                if (racers.TrueForAll(racer => racer.isFinished))
                {
                    Debug.Log("All reached goal");
                }
            }
        }
        else
        {
            playerRacer.IncrementLoopCount();
            race.currectLoop++;
            GameController.Instance.StartNewRound();
            if (playerRacer.name.Equals(racers[racerToDisplay].name))
            {
                UpdateRaceTrackNumber(playerRacer.LoopCount);
                if (displayCanvas)
                {
                    roundTextComponent.text =
                        "Round: " + playerRacer.LoopCount + "/" + race.loopsPerGame;
                }
            }
        }

        return playerRacer;
    }

    public void IncrementLoopNumber(PlayerController playerController)
    {
        playerController.racer = IncrementLoopNumberOnRacer(playerController.racer);
    }

    public void IncrementLoopNumber(PlayerSimulationController playerSimulationController)
    {
        playerSimulationController.racer = IncrementLoopNumberOnRacer(playerSimulationController.racer);
    }
}
