﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Race
{
    public int currectLoop;
    public int loopsPerGame;
    
    public Race(int currectLoop, int loopsPerGame)
    {
        this.currectLoop = currectLoop;
        this.loopsPerGame = loopsPerGame;
    }

    public Race(int loopsPerGame)
    {
        this.loopsPerGame = loopsPerGame;
    }
}
