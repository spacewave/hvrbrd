﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Google.Protobuf.WellKnownTypes;
using Sabresaurus.SabreCSG;
using UnityEngine;
using UnityEngine.UI;
using static SettingsUtil;
using Debug = UnityEngine.Debug;

public class GameController : MonoBehaviour
{
    private static GameController _instance;

    public static GameController Instance
    {
        get { return _instance; }
    }

    private Settings _settings;
    public Canvas canvas;
    public GameObject roundTextPrefab;
    public List<Text> texts;
    public UnityTimeStopwatch roundStopwatch;
    public bool endGame = false;
    public UnityTimeStopwatch globalStopWatch;
	public float offset = 20f;
	public GameObject timeSlot;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
        _settings = ReadSettings();
        UpdateSettings(_settings);
        globalStopWatch = new UnityTimeStopwatch();
        StartClock(globalStopWatch);
        roundStopwatch = new UnityTimeStopwatch();
    }

    public void StartNewRound()
    {
        if (RaceController.Instance.displayCanvas)
        {
            roundStopwatch.HasEnded = true;
            roundStopwatch = new UnityTimeStopwatch();
            StartClock(roundStopwatch);
            GameObject textGameObject = Instantiate(roundTextPrefab, timeSlot.transform);
            Vector3 textPosition = textGameObject.GetComponent<RectTransform>().position;
            textGameObject.GetComponent<RectTransform>().localPosition = new Vector3(-50f, textPosition.y - (RaceController.Instance.race.currectLoop) * offset, 0f);
            texts.Add(textGameObject.GetComponent<Text>());
        }
    }

    private void FixedUpdate()
    {
		if(roundStopwatch != null){
			if (roundStopwatch.IsStarted && !endGame)
			{
				UpdateStopWatchText(texts[RaceController.Instance.race.currectLoop - 1]);
			}			
		}
    }

    private void UpdateStopWatchText(Text text)
    {
        if (!RaceController.Instance.displayCanvas) return;
        TimeSpan timeSpan = roundStopwatch.GetElapsedTime();
        string timeTextString = "";
        timeTextString += roundStopwatch.GetElapsedTime().Minutes + ":";
        timeTextString += roundStopwatch.GetElapsedTime().Seconds + ":";
        timeTextString += roundStopwatch.GetElapsedTime().Milliseconds;
        text.text = timeTextString;
    }

    IEnumerator UpdateClock(UnityTimeStopwatch unityTimeStopwatch)
    {
        while (!unityTimeStopwatch.HasEnded)
        {
            unityTimeStopwatch.milliseconds += (Time.deltaTime * 1000) % 1000;

            if (unityTimeStopwatch.seconds > 59)
            {
                unityTimeStopwatch.minutes++;
                unityTimeStopwatch.seconds = 0;
            }

            if (unityTimeStopwatch.minutes > 59)
            {
                unityTimeStopwatch.hours++;
                unityTimeStopwatch.minutes = 0;
            }

            if (unityTimeStopwatch.hours > 23)
            {
                unityTimeStopwatch.days++;
                unityTimeStopwatch.hours = 0;
            }
            yield return null;
        }
    }


    public void StartClock(UnityTimeStopwatch unityTimeStopwatch)
    {
        if (!unityTimeStopwatch.IsStarted)
            unityTimeStopwatch.IsStarted = true;
        StartCoroutine(UpdateClock(unityTimeStopwatch));
    }


    
}

public enum GameType
{
    Time,
    Race
}

public class UnityTimeStopwatch
{
    public float milliseconds = 0f;
    public float seconds = 0f;
    public int minutes = 0;
    public int hours = 0;
    public int days = 0;
    public bool IsStarted { get; set; } = false;
    public bool HasEnded { get; set; } = false;
    
    public TimeSpan GetElapsedTime()
    {
        return new TimeSpan(days, hours, minutes, (int) seconds, (int) milliseconds);
        ;
    }
}