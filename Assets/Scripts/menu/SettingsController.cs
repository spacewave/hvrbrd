﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using Barracuda;
using Google.Protobuf;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsController : MonoBehaviour
{
    public static SettingsController Instance { get; private set; }
    
    private const string SETTINGS_DIR = "./settings.json";

    public GameObject playerPrefab;
    
    public Scene timeRaceScene;
    public Settings settings;
    public Scene raceScene;
    public string loadScene;

    private float _audioVolume;
    private float _soundVolume;
    private bool _simulationControls;
    private bool _wiiFitBoardsControls;
    private bool _extendedViveControls;

    public bool SimulationControls
    {
        get => _simulationControls;
        set
        {
            _simulationControls = value;
            settings.SimulationControls = value;
        }
    }

    public bool WiiFitBoardsControls
    {
        get => _wiiFitBoardsControls;
        set
        {
            _wiiFitBoardsControls = value;
            settings.WiiFitBoardsControls = value;
        }
    }

    public bool ExtendedViveControls
    {
        get => _extendedViveControls;
        set
        {
            _extendedViveControls = value;
            settings.ExtendedViveControls = value;
        }
    }

    public float AudioVolume
    {
        get => _audioVolume;
        set
        {
            settings.AudioVolume = value;
            _audioVolume = value;
        }
    }

    public float SoundVolume
    {
        get => _soundVolume;
        set
        {
            settings.SoundVolume = value;
            _soundVolume = value;
        }
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void SetPlayerName(string playerName)
    {
        PlayerPrefs.SetString("name", playerName);
        PlayerPrefs.Save();
    }
    
    public void Start()
    {
        
        FileStream fileStream;
        settings = new Settings();

        if (!File.Exists(SETTINGS_DIR))
        {
            fileStream = new FileStream(SETTINGS_DIR, FileMode.Create);
        }
        else
        {
            fileStream = new FileStream(SETTINGS_DIR, FileMode.Open);
        }

        fileStream.Close();
        WriteSettings();
    }

    public void WriteSettings()
    {
        StreamWriter streamWriter = new StreamWriter(SETTINGS_DIR);
        streamWriter.Write(JsonUtility.ToJson(settings));
        streamWriter.Flush();
        streamWriter.Close();
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void setLoadScene(string sceneName)
    {
        loadScene = sceneName;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(loadScene);
    }
}

public static class SettingsUtil
{
    private const string SETTINGS_DIR = "./settings.json";

    public static Settings ReadSettings()
    {
        StreamReader streamReader = new StreamReader(SETTINGS_DIR);
        string read = streamReader.ReadToEnd();
        Settings readSettings = JsonUtility.FromJson<Settings>(read);
        return readSettings;
    }

    public static void UpdateSettings(Settings settings)
    {
        AudioSource backTheme = RaceController.Instance.GetComponent<AudioSource>();
        if (backTheme != null)
        {
            backTheme.volume = settings.SoundVolume * backTheme.volume;
        }

        GameObject.FindGameObjectsWithTag("board").ToList().ForEach(o =>
            o.GetComponents<AudioSource>().ToList().ForEach(audioSource =>
            {
                audioSource.volume = settings.SoundVolume * audioSource.volume;
            }));

        GameObject.FindGameObjectsWithTag("spawner").ToList().ForEach(o =>
            o.GetComponent<RaceTrackPlayerSpawner>().spawnPlayerSimulation = !settings.WiiFitBoardsControls);
    }
}