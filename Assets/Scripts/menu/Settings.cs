﻿using System;
using System.Collections;
using System.Collections.Generic;
using Google.Protobuf;
using Google.Protobuf.Reflection;
using UnityEngine;
using Object = System.Object;

[Serializable]
public class Settings : Object
{
    [SerializeField]
    private float audioVolume; 
    public float AudioVolume
    {
        set
        {
            audioVolume = value;
            SettingsController.Instance.WriteSettings();
        }
        get => audioVolume;
    }
    
    [SerializeField]
    private float soundVolume;

    public float SoundVolume
    {
        get => soundVolume;
        set
        {
            soundVolume = value;
            SettingsController.Instance.WriteSettings();
        } 
            
    }
    
    [SerializeField] private bool simulationControls;

    public bool SimulationControls
    {
        get => simulationControls;
        set
        {
            simulationControls = value;
            SettingsController.Instance.WriteSettings();
        }
    }

    [SerializeField]
    private bool wiiFitBoardsControls;
    public bool WiiFitBoardsControls
    {
        get => wiiFitBoardsControls;
        set
        {
            wiiFitBoardsControls = value;
            SettingsController.Instance.WriteSettings();
        }
    }

    [SerializeField]
    private bool extendedViveControls;
    public bool ExtendedViveControls
    {
        get => extendedViveControls;
        set
        {
            extendedViveControls = value;
            SettingsController.Instance.WriteSettings();
        }
    }

    public MessageDescriptor Descriptor { get; }
}
