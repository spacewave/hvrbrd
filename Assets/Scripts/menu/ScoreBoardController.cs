﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Google.Protobuf.WellKnownTypes;
using Sabresaurus.SabreCSG;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class ScoreBoardController : MonoBehaviour
{
    private const string SCORE_PATH = "./high_score.json";
    public ScoreBoard scoreBoard;
    public Text scoreBoardNames;
    public Text scoreBoardScores;

    public void Start()
    {
        scoreBoard = ScoreBoardUtil.fetchScoreBoard();
        WriteScoreBoard();
    }

    private void WriteScoreBoard()
    {
        scoreBoardNames.text = "";
        scoreBoardScores.text = "";
        int rank = 1;
        scoreBoard.scoreEntries.ForEach(entry =>
        {
            scoreBoardNames.text += rank + "." + entry.name + "\n";
            scoreBoardScores.text +=
                entry.hours + ":" + entry.minutes + ":" + entry.seconds + ":" + entry.mseconds + "\n";
            rank += 1;
        });
    }
}

[Serializable]
public class ScoreBoard
{
    public List<ScoreEntry> scoreEntries;
}

public static class ScoreBoardUtil
{
    private const string SCORE_PATH = "./high_score.json";
    public static ScoreBoard fetchScoreBoard()
    {
        FileStream fileStream;

        if (!File.Exists(SCORE_PATH))
        {
            fileStream = new FileStream(SCORE_PATH, FileMode.Create);
        }
        else
        {
            fileStream = new FileStream(SCORE_PATH, FileMode.Open);
        }
        StreamReader streamReader = new StreamReader(fileStream);
        string read = streamReader.ReadToEnd();
        ScoreBoard readBoard = JsonUtility.FromJson<ScoreBoard>(read);
        streamReader.Close();
        return readBoard;
    }
    
    public static void ExportScoreBoard(ScoreBoard scoreBoard)
    {
        StreamWriter streamWriter = new StreamWriter(SCORE_PATH);
        scoreBoard.scoreEntries.Sort();
        if (scoreBoard.scoreEntries.Count > 9)
        {
            scoreBoard.scoreEntries = scoreBoard.scoreEntries.GetRange(0, 10);
        }
        streamWriter.Write(JsonUtility.ToJson(scoreBoard));
        streamWriter.Flush();
        streamWriter.Close();
    }
}

[Serializable]
public class ScoreEntry : IComparable<ScoreEntry>
{
    public string name;
    public int hours;
    public int minutes;
    public int seconds;
    public int mseconds;

    public ScoreEntry(string name, int hours, int minutes, int seconds, int mseconds)
    {
        this.name = name;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.mseconds = mseconds;
    }

    public int CompareTo(ScoreEntry other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        var hoursComparison = hours.CompareTo(other.hours);
        if (hoursComparison != 0) return hoursComparison;
        var minutesComparison = minutes.CompareTo(other.minutes);
        if (minutesComparison != 0) return minutesComparison;
        var secondsComparison = seconds.CompareTo(other.seconds);
        if (secondsComparison != 0) return secondsComparison;
        return mseconds.CompareTo(other.mseconds);
    }
}


