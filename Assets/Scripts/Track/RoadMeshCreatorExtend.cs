﻿using System;
using System.Collections.Generic;
using System.Linq;
using PathCreation;
using PathCreation.Examples;
using UnityEngine;
using PathCreation.Utility;
using UnityEditor;

namespace PathCreation.Examples
{
    public class RoadMeshCreatorExtend : PathSceneTool
    {
        [Header("Road settings")] public float roadWidth = .4f;

        public float roadSideWidth = 0.1f;
        public float roadSideBarrierHeight = 3f;
        [Range(0, .5f)] public float thickness = .15f;
        public bool flattenSurface;

        [Header("Material settings")] public Material roadMaterial;
        public Material undersideMaterial;
        public Material barrierMaterial;
        public float textureTiling = 1;

        MeshFilter _roadMeshFilter;
        MeshRenderer _roadMeshRenderer;

        MeshFilter _rightRoadBarrierMeshFilter;
        MeshRenderer _rightRoadBarrierMeshRenderer;

        MeshFilter _leftRoadBarrierMeshFilter;
        MeshRenderer _leftRoadBarrierMeshRenderer;
        private MeshCollider _leftMeshCollider;
        private MeshCollider _rightMeshCollider;
        private MeshCollider _meshRoadCollider;
        
        private VertexPath _vertexPath;
        public VertexPath VertexPath => _vertexPath;

        public RaceTrackStartSpawner raceTrackStartSpawner;
        public RaceTrackCheckpointSpawner raceTrackCheckpointSpawner;
        
        protected override void PathUpdated()
        {
            if (pathCreator != null)
            {
                AssignMeshComponents("road_mesh", "ground", out _roadMeshFilter, out _roadMeshRenderer, out _meshRoadCollider);
                AssignMeshComponents("right_road_barrier_mesh", "right_bounds", out _rightRoadBarrierMeshFilter,
                    out _rightRoadBarrierMeshRenderer, out _rightMeshCollider);
                AssignMeshComponents("left_road_barrier_mesh", "left_bounds" , out _leftRoadBarrierMeshFilter,
                    out _leftRoadBarrierMeshRenderer, out _leftMeshCollider);
                AssignMaterials(out _roadMeshRenderer, _roadMeshFilter.transform, roadMaterial, undersideMaterial);
                AssignMaterials(out _rightRoadBarrierMeshRenderer, _rightRoadBarrierMeshFilter.transform,
                    barrierMaterial, undersideMaterial);
                AssignMaterials(out _leftRoadBarrierMeshRenderer, _leftRoadBarrierMeshFilter.transform, barrierMaterial,
                    undersideMaterial);
                
                _roadMeshFilter.sharedMesh = CreateRoadMesh();
                _rightRoadBarrierMeshFilter.sharedMesh = CreateRoadSideMesh(false);
                _leftRoadBarrierMeshFilter.sharedMesh = CreateRoadSideMesh(true);

                _meshRoadCollider.sharedMesh = _roadMeshFilter.sharedMesh;
                _rightMeshCollider.sharedMesh = _rightRoadBarrierMeshFilter.sharedMesh;
                _leftMeshCollider.sharedMesh = _leftRoadBarrierMeshFilter.sharedMesh;
                
                if (raceTrackStartSpawner != null)
                {
                    raceTrackStartSpawner.path = path;
                    raceTrackStartSpawner.SpawnCheckPoints();
                }

                if (raceTrackCheckpointSpawner != null)
                {
                    raceTrackCheckpointSpawner.path = path;
                    raceTrackCheckpointSpawner.DestroyExistingCheckPoints();
                    raceTrackCheckpointSpawner.SpawnAiCheckPoints();
                }
                
            }
        }

        private Mesh CreateRoadSideMesh(bool isLeft)
        {
            Vector3[] verts = new Vector3[path.NumVertices * 8];
            Vector2[] uvs = new Vector2[verts.Length];
            Vector3[] normals = new Vector3[verts.Length];

            int numTris = 2 * (path.NumVertices - 1) + 2;
            int[] roadTriangles = new int[numTris * 3];
            int[] underRoadTriangles = new int[numTris * 3];
            int[] sideOfRoadTriangles = new int[numTris * 2 * 3];


            int vertIndex = 0;
            int triIndex = 0;

            // Vertices for the top of the road are layed out:
            // 0  1
            // 8  9
            // and so on... So the triangle map 0,8,1 for example, defines a triangle from top left to bottom left to bottom right.
            int[] triangleMap = {0, 8, 1, 1, 8, 9};
            int[] sidesTriangleMap = {4, 6, 14, 12, 4, 14, 5, 15, 7, 13, 15, 5};

            bool usePathNormals = !(path.space == PathSpace.xyz && flattenSurface);

            for (int i = 0; i < path.NumVertices; i++)
            {
                Vector3 localUp = (usePathNormals) ? Vector3.Cross(path.tangents[i], path.normals[i]) : path.up;
                Vector3 localRight = (usePathNormals) ? path.normals[i] : Vector3.Cross(localUp, path.tangents[i]);

                // Find position to left and right of current path vertex 
                Vector3 vertSideA = Vector3.zero;
                if (isLeft)
                {
                    vertSideA = path.vertices[i] - Mathf.Abs(roadWidth) * localRight - transform.position -
                                Mathf.Abs(roadSideWidth) * localRight;
                }
                else
                {
                    vertSideA = path.vertices[i] + Mathf.Abs(roadWidth) * localRight - transform.position;
                }

                Vector3 vertSideB = vertSideA + Mathf.Abs(roadSideWidth) * localRight;

                // Add top of road vertices
                verts[vertIndex + 0] = vertSideA + (isLeft ? roadSideBarrierHeight : 1) * thickness * localUp;
                verts[vertIndex + 1] = vertSideB + (isLeft ? 1 : roadSideBarrierHeight) * thickness * localUp;
                // Add bottom of road vertices
                verts[vertIndex + 2] = vertSideA - thickness * localUp;
                verts[vertIndex + 3] = vertSideB - thickness * localUp;

                // Duplicate vertices to get flat shading for sides of road
                verts[vertIndex + 4] = verts[vertIndex + 0];
                verts[vertIndex + 5] = verts[vertIndex + 1];
                verts[vertIndex + 6] = verts[vertIndex + 2];
                verts[vertIndex + 7] = verts[vertIndex + 3];

                // Set uv on y axis to path time (0 at start of path, up to 1 at end of path)
                uvs[vertIndex + 0] = new Vector2(0, path.times[i]);
                uvs[vertIndex + 1] = new Vector2(1, path.times[i]);

                // Top of road normals
                normals[vertIndex + 0] = localUp;
                normals[vertIndex + 1] = localUp;
                // Bottom of road normals
                normals[vertIndex + 2] = -localUp;
                normals[vertIndex + 3] = -localUp;
                // Sides of road normals
                normals[vertIndex + 4] = -localRight;
                normals[vertIndex + 5] = localRight;
                normals[vertIndex + 6] = -localRight;
                normals[vertIndex + 7] = localRight;

                // Set triangle indices
                if (i < path.NumVertices - 1 || path.isClosedLoop)
                {
                    for (int j = 0; j < triangleMap.Length; j++)
                    {
                        roadTriangles[triIndex + j] = (vertIndex + triangleMap[j]) % verts.Length;
                        // reverse triangle map for under road so that triangles wind the other way and are visible from underneath
                        underRoadTriangles[triIndex + j] =
                            (vertIndex + triangleMap[triangleMap.Length - 1 - j] + 2) % verts.Length;
                    }

                    for (int j = 0; j < sidesTriangleMap.Length; j++)
                    {
                        sideOfRoadTriangles[triIndex * 2 + j] = (vertIndex + sidesTriangleMap[j]) % verts.Length;
                    }
                }


                vertIndex += 8;
                triIndex += 6;
            }

            Mesh mesh = new Mesh();
            mesh.vertices = verts;
            mesh.uv = uvs;
            mesh.normals = normals;
            mesh.subMeshCount = 3;
            mesh.SetTriangles(roadTriangles, 0);
            mesh.SetTriangles(underRoadTriangles, 1);
            mesh.SetTriangles(sideOfRoadTriangles, 2);
            mesh.RecalculateBounds();

            return mesh;
        }


        Mesh CreateRoadMesh()
        {
            Vector3[] verts = new Vector3[path.NumVertices * 8];
            Vector2[] uvs = new Vector2[verts.Length];
            Vector3[] normals = new Vector3[verts.Length];

            int numTris = 2 * (path.NumVertices - 1) + ((path.isClosedLoop) ? 2 : 0);
            int[] roadTriangles = new int[numTris * 3];
            int[] underRoadTriangles = new int[numTris * 3];
            int[] sideOfRoadTriangles = new int[numTris * 2 * 3];

            int vertIndex = 0;
            int triIndex = 0;

            // Vertices for the top of the road are layed out:
            // 0  1
            // 8  9
            // and so on... So the triangle map 0,8,1 for example, defines a triangle from top left to bottom left to bottom right.
            int[] triangleMap = {0, 8, 1, 1, 8, 9};
            int[] sidesTriangleMap = {4, 6, 14, 12, 4, 14, 5, 15, 7, 13, 15, 5};

            bool usePathNormals = !(path.space == PathSpace.xyz && flattenSurface);

            for (int i = 0; i < path.NumVertices; i++)
            {
                Vector3 localUp = (usePathNormals) ? Vector3.Cross(path.tangents[i], path.normals[i]) : path.up;
                Vector3 localRight = (usePathNormals) ? path.normals[i] : Vector3.Cross(localUp, path.tangents[i]);

                // Find position to left and right of current path vertex 
                Vector3 vertSideA = path.vertices[i] - localRight * Mathf.Abs(roadWidth) - transform.position;
                Vector3 vertSideB = path.vertices[i] + localRight * Mathf.Abs(roadWidth) - transform.position;

                // Add top of road vertices
                verts[vertIndex + 0] = vertSideA + localUp * thickness;
                verts[vertIndex + 1] = vertSideB + localUp * thickness;
                // Add bottom of road vertices
                verts[vertIndex + 2] = vertSideA - localUp * thickness;
                verts[vertIndex + 3] = vertSideB - localUp * thickness;

                // Duplicate vertices to get flat shading for sides of road
                verts[vertIndex + 4] = verts[vertIndex + 0];
                verts[vertIndex + 5] = verts[vertIndex + 1];
                verts[vertIndex + 6] = verts[vertIndex + 2];
                verts[vertIndex + 7] = verts[vertIndex + 3];

                // Set uv on y axis to path time (0 at start of path, up to 1 at end of path)
                uvs[vertIndex + 0] = new Vector2(0, path.times[i]);
                uvs[vertIndex + 1] = new Vector2(1, path.times[i]);

                // Top of road normals
                normals[vertIndex + 0] = localUp;
                normals[vertIndex + 1] = localUp;
                // Bottom of road normals
                normals[vertIndex + 2] = -localUp;
                normals[vertIndex + 3] = -localUp;
                // Sides of road normals
                normals[vertIndex + 4] = -localRight;
                normals[vertIndex + 5] = localRight;
                normals[vertIndex + 6] = -localRight;
                normals[vertIndex + 7] = localRight;


                // Set triangle indices
                if (i < path.NumVertices - 1 || path.isClosedLoop)
                {
                    for (int j = 0; j < triangleMap.Length; j++)
                    {
                        roadTriangles[triIndex + j] = (vertIndex + triangleMap[j]) % verts.Length;
                        // reverse triangle map for under road so that triangles wind the other way and are visible from underneath
                        underRoadTriangles[triIndex + j] =
                            (vertIndex + triangleMap[triangleMap.Length - 1 - j] + 2) % verts.Length;
                    }

                    for (int j = 0; j < sidesTriangleMap.Length; j++)
                    {
                        sideOfRoadTriangles[triIndex * 2 + j] = (vertIndex + sidesTriangleMap[j]) % verts.Length;
                    }
                }

                vertIndex += 8;
                triIndex += 6;
            }

            Mesh mesh = new Mesh();
            mesh.vertices = verts;
            mesh.uv = uvs;
            mesh.normals = normals;
            mesh.subMeshCount = 3;
            mesh.SetTriangles(roadTriangles, 0);
            mesh.SetTriangles(underRoadTriangles, 1);
            mesh.SetTriangles(sideOfRoadTriangles, 2);
            mesh.RecalculateBounds();

            return mesh;
        }


        // Add MeshRenderer and MeshFilter components to this gameobject if not already attached
        void AssignMeshComponents(string meshHolderName, string meshHolderTag, out MeshFilter meshFilter, out MeshRenderer meshRenderer, out MeshCollider meshCollider)
        {
            Transform meshHolder = transform.Find(meshHolderName);

            if (meshHolder == null)
            {
                meshHolder = new GameObject(meshHolderName).transform;
                meshHolder.gameObject.tag = meshHolderTag;
                meshHolder.transform.parent = transform;
                meshHolder.transform.localPosition = Vector3.zero;
            }


            //meshHolder.transform.position = Vector3.zero;
            meshHolder.transform.rotation = Quaternion.identity;

            // Ensure mesh renderer and filter components are assigned
            if (!meshHolder.gameObject.GetComponent<MeshFilter>())
            {
                meshHolder.gameObject.AddComponent<MeshFilter>();
            }

            if (!meshHolder.GetComponent<MeshRenderer>())
            {
                meshHolder.gameObject.AddComponent<MeshRenderer>();
            }
            
            if (!meshHolder.GetComponent<MeshCollider>())
            {
                meshHolder.gameObject.AddComponent<MeshCollider>();
            }

            meshRenderer = meshHolder.GetComponent<MeshRenderer>();
            meshFilter = meshHolder.GetComponent<MeshFilter>();
            meshCollider = meshHolder.GetComponent<MeshCollider>();
        }

        void AssignMaterials(out MeshRenderer meshRenderer, Transform meshHolder, Material material,
            Material bottomMaterial)
        {
            meshRenderer = meshHolder.GetComponent<MeshRenderer>();

            if (material != null && bottomMaterial != null && meshRenderer != null)
            {
                meshRenderer.sharedMaterials = new Material[] {material, bottomMaterial, bottomMaterial};
                meshRenderer.sharedMaterials[0].mainTextureScale = new Vector3(1, textureTiling);
            }
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(RoadMeshCreatorExtend))]
public class RoadMeshCreatorExtendEditor : Editor
{
    protected PathSceneTool pathTool;
    bool isSubscribed;

    public override void OnInspectorGUI()
    {
        using (var check = new EditorGUI.ChangeCheckScope())
        {
            DrawDefaultInspector();

            if (check.changed)
            {
                if (!isSubscribed)
                {
                    TryFindPathCreator();
                    Subscribe();
                }

                if (pathTool.autoUpdate)
                {
                    pathTool.CreatePath();
                }
            }
        }

        if (GUILayout.Button("Manual Update"))
        {
            if (TryFindPathCreator())
            {
                pathTool.CreatePath();
                SceneView.RepaintAll();
            }
        }
    }


    protected virtual void OnPathModified()
    {
        if (pathTool.autoUpdate)
        {
            pathTool.CreatePath();
        }
    }

    protected virtual void OnEnable()
    {
        pathTool = (PathSceneTool) target;
        pathTool.onDestroyed += OnToolDestroyed;

        if (TryFindPathCreator())
        {
            Subscribe();
            pathTool.CreatePath();
        }
    }

    void OnToolDestroyed()
    {
        if (pathTool != null)
        {
            pathTool.pathCreator.pathUpdated -= OnPathModified;
        }
    }


    protected virtual void Subscribe()
    {
        if (pathTool.pathCreator != null)
        {
            isSubscribed = true;
            pathTool.pathCreator.pathUpdated -= OnPathModified;
            pathTool.pathCreator.pathUpdated += OnPathModified;
        }
    }

    bool TryFindPathCreator()
    {
        // Try find a path creator in the scene, if one is not already assigned
        if (pathTool.pathCreator == null)
        {
            if (pathTool.GetComponent<PathCreator>() != null)
            {
                pathTool.pathCreator = pathTool.GetComponent<PathCreator>();
            }
            else if (FindObjectOfType<PathCreator>())
            {
                pathTool.pathCreator = FindObjectOfType<PathCreator>();
            }
        }

        return pathTool.pathCreator != null;
    }
}
#endif