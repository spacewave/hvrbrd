﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointBehavior : MonoBehaviour
{
    public Checkpoint checkpoint;
    public CheckPointType checkPointType;
    public bool hasDrivenThroughCheckpoint = false;

    private void OnTriggerEnter(Collider col)
    {
        if (col.attachedRigidbody.tag.Equals("board"))
        {
            if(checkPointType != CheckPointType.Orientation)
                col.GetComponentInParent<RaceTrackStart>().ComputeCollision(checkPointType, col);
            else
            {
                if (col.gameObject.GetComponent<PlayerController>() != null)
                {
                    checkpoint.nextCheckPoint.GetComponent<Renderer>().material.color =
                        col.GetComponent<PlayerController>().racer.color;
                } 
                if (col.gameObject.GetComponent<HVRAgent>() != null)
                {
                    if(checkpoint.nextCheckPoint != null)
                    {
                        checkpoint.nextCheckPoint.GetComponent<Renderer>().material.color =
                        col.GetComponent<HVRAgent>().racer.color;}

                    if (!hasDrivenThroughCheckpoint)
                    {
                        hasDrivenThroughCheckpoint = true;
                    }               
                }
            }
        }
    }
}

public enum CheckPointType
{
    Start,
    Finish,
    Orientation
}

[Serializable]
public class Checkpoint
{
    public int id;
    public GameObject checkpointGameObject;
    public GameObject nextCheckPoint;

    public Checkpoint(int id, GameObject checkpointGameObject)
    {
        this.id = id;
        this.checkpointGameObject = checkpointGameObject;
    }
    
    
    
}