﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PathCreation;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class RaceTrackStartSpawner : MonoBehaviour
{
    public int indexOfTrackStart;
    public int indexOfTrackEnd;

    public GameObject trackEndPrefab;
    public GameObject trackStartPrefab;
    public GameObject trackEnd;
    public GameObject trackStart;
    public VertexPath path;
    public float gapBetweenStartAndEndFaktor = 2f;

    public void SpawnCheckPoints()
    {
        if (trackStartPrefab != null && trackStartPrefab != null)
        {
            Vector3 trackStartPosition = (path.vertices[indexOfTrackEnd] + path.vertices[indexOfTrackEnd + 1]) / 2 +
                                       new Vector3(0, trackEndPrefab.transform.position.y, 0);
            Vector3 trackOriginPosition =
                (path.vertices[indexOfTrackStart] + path.vertices[indexOfTrackStart + 1]) / 2 +
                new Vector3(0, trackStartPosition.y, 0);

            Vector3 directionVector =
                new Vector3(trackOriginPosition.x, 0, trackOriginPosition.z) -
                path.vertices[indexOfTrackStart + 1];

            Vector3 trackEndPosition = (trackOriginPosition + directionVector.normalized * gapBetweenStartAndEndFaktor);

            if (trackEnd == null)
            {
                trackEnd = Instantiate(trackEndPrefab,trackEndPosition,
                    Quaternion.LookRotation(Vector3.Cross(
                        path.vertices[indexOfTrackEnd + 1] - path.vertices[indexOfTrackEnd],
                        Vector3.up)), transform);
            }
            else
            {
                trackEnd.transform.position = trackEndPosition;
                trackEnd.transform.rotation = Quaternion.LookRotation(Vector3.Cross(
                    path.vertices[indexOfTrackEnd + 1] - path.vertices[indexOfTrackEnd],
                    Vector3.up));
            }

            if (trackStart == null)
            {
                trackStart = Instantiate(trackStartPrefab, trackStartPosition,
                    Quaternion.LookRotation(Vector3.Cross(
                        path.vertices[indexOfTrackStart + 1] - path.vertices[indexOfTrackStart],
                        Vector3.up)), transform);
            }
            else
            {
                trackStart.transform.position = trackStartPosition;
                trackStart.transform.rotation = Quaternion.LookRotation(Vector3.Cross(
                    path.vertices[indexOfTrackStart + 1] - path.vertices[indexOfTrackStart],
                    Vector3.up));
            }
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(RaceTrackStartSpawner))]
public class RaceTrackStartSpawnerEditor : Editor
{
    private List<string> _choices = new List<string>();
    private int _startChoiceIndex = 0;
    private int _endChoiceIndex = 0;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        RaceTrackStartSpawner someClass = target as RaceTrackStartSpawner;

        foreach (int i in Enumerable.Range(0, someClass.GetComponent<PathCreator>().path.vertices.Length - 1))
        {
            _choices.Add(i.ToString());
        }

        _startChoiceIndex = someClass.indexOfTrackStart;
        _endChoiceIndex = someClass.indexOfTrackEnd;

        _startChoiceIndex = EditorGUILayout.Popup(_startChoiceIndex, _choices.ToArray());
        _endChoiceIndex = EditorGUILayout.Popup(_endChoiceIndex, _choices.ToArray());

        // Update the selected choice in the underlying object
        someClass.indexOfTrackStart = int.Parse(_choices[_startChoiceIndex]);
        someClass.indexOfTrackEnd = int.Parse(_choices[_endChoiceIndex]);
        // Save the changes back to the object
        EditorUtility.SetDirty(target);
    }
}
#endif