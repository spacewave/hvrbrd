﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PathCreation;
using UnityEngine;
using UnityEngine.Serialization;

public class RaceTrackCheckpointSpawner : MonoBehaviour
{
    public GameObject checkPointPrefab;
    public VertexPath path;
    public float checkpointDistance;
    public List<Checkpoint> checkpoints;
    public Transform checkPointParentTrans;
    public bool aiCheckPointsNotVisible = false;
    private bool _initialRun;

    public void DestroyExistingCheckPoints()
    {
        checkPointParentTrans.GetComponentsInChildren<CheckPointBehavior>().ToList()
            .ForEach(behavior => DestroyImmediate(behavior.gameObject, true));
        checkpoints.Clear();
    }

    public void SpawnAiCheckPoints()
    {
        GameObject checkpointGameObject;
        int checkpointId = 0;
        int counter = 0;
        float sum = 0;
        Dictionary<int, KeyValuePair<Vector3, Quaternion>> checkPointPositions =
            new Dictionary<int, KeyValuePair<Vector3, Quaternion>>();

        for (int i = 0; i < path.vertices.Length; i++)
        {
            sum += (path.vertices[((i + 1) % path.vertices.Length)] - path.vertices[i]).magnitude;
        }

        float average = sum / path.vertices.Length;

        for (int i = 0; i < path.vertices.Length; i++)
        {
            checkPointPositions.Add(counter, new KeyValuePair<Vector3, Quaternion>(
                path.vertices[i] + new Vector3(0, checkPointPrefab.transform.position.y, 0),
                Quaternion.LookRotation(Vector3.Cross(
                    path.vertices[((i + 1) % path.vertices.Length)] - path.vertices[i],
                    Vector3.up))));
            counter++;
            collectCheckpointsForBigDistances(average, i, out i, counter, out counter, checkPointPositions,
                out checkPointPositions);
        }

        List<KeyValuePair<int, KeyValuePair<Vector3, Quaternion>>>
            sortedCheckPointSpawns = checkPointPositions.ToList();
        sortedCheckPointSpawns.Sort((pair, valuePair) =>
        {
            if (pair.Key < valuePair.Key)
            {
                return 1;
            }

            if (pair.Key > valuePair.Key)
            {
                return -1;
            }

            return 0;
        });


        sortedCheckPointSpawns.ForEach(pair =>
        {
            if (pair.Key % 2 != 0) return;
            checkpointGameObject = Instantiate(checkPointPrefab,
                pair.Value.Key, pair.Value.Value, checkPointParentTrans);
            checkpointGameObject.GetComponent<CheckPointBehavior>().checkpoint.id = pair.Key / 2;
            checkpointGameObject.GetComponent<CheckPointBehavior>().checkpoint.checkpointGameObject =
                checkpointGameObject;
            checkpoints.Add(checkpointGameObject.GetComponent<CheckPointBehavior>().checkpoint);
        });

        for (int i = 0; i < checkpoints.Count; i++)
        {
            checkpoints[i].nextCheckPoint = checkpoints[(i + 1) % checkpoints.Count].checkpointGameObject;
            checkpoints[i].checkpointGameObject.GetComponent<CheckPointBehavior>().checkpoint.nextCheckPoint =
                checkpoints[(i + 1) % checkpoints.Count].checkpointGameObject;
        }

        GetComponent<RaceTrackStart>().checkPoints = checkpoints;
    }

    private void collectCheckpointsForBigDistances(float average, int iOld, out int i, int counterOld, out int counter,
        Dictionary<int, KeyValuePair<Vector3, Quaternion>> checkPointPositionsOld,
        out Dictionary<int, KeyValuePair<Vector3, Quaternion>> checkPointPositions)

    {
        i = iOld;
        counter = counterOld;
        checkPointPositions = checkPointPositionsOld;

        if ((path.vertices[((i + 1) % path.vertices.Length)] - path.vertices[i]).magnitude >
            average)
        {
            for (int j =
                    Mathf.CeilToInt((path.vertices[((i + 1) % path.vertices.Length)] -
                                     path.vertices[i])
                                    .magnitude / average);
                j > 0;
                j--)
            {
                if (i == path.vertices.Length - 1 && !path.isClosedLoop)
                {
                }
                else
                {
                    checkPointPositions.Add(counter + j, new KeyValuePair<Vector3, Quaternion>(
                        path.vertices[i] + j * checkpointDistance *
                        (path.vertices[((i + 1) % path.vertices.Length)] -
                         path.vertices[i]).normalized + new Vector3(0, checkPointPrefab.transform.position.y, 0),
                        Quaternion.LookRotation(Vector3.Cross(
                            path.vertices[((i + 1) % path.vertices.Length)] - path.vertices[i],
                            Vector3.up))));
                }
            }

            counter += Mathf.CeilToInt((path.vertices[((i + 1) % path.vertices.Length)] -
                                        path.vertices[i])
                                       .magnitude / average) + 1;
        }
    }
}