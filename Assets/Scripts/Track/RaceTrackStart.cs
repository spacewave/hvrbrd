﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RaceTrackStart : MonoBehaviour
{
    public bool hasCollidedWithStart = false;
    public List<Checkpoint> checkPoints;

    public void Start()
    {
        checkPoints = GetComponent<RaceTrackCheckpointSpawner>().checkpoints;
    }

    public void ComputeCollision(CheckPointType checkPointType, Collider col)
    {
        if (col.GetComponent<RacerPosition>().hasCollidedWithStart && checkPointType == CheckPointType.Finish)
        {
            if (col.GetComponent<PlayerController>() != null)
            {
                if (RaceController.Instance != null)
                {
                    RaceController.Instance.IncrementLoopNumber(col.GetComponent<PlayerController>());
                }
            }
            else if (col.GetComponent<PlayerSimulationController>() != null)
            {
                if (RaceController.Instance != null)
                {
                    RaceController.Instance.IncrementLoopNumber(col.GetComponent<PlayerSimulationController>());
                }
            }
            else
            {
                if (RaceController.Instance != null)
                {
                    RaceController.Instance.IncrementLoopNumber(col.GetComponent<HVRAgent>());
                }
                else
                {
                    if (hasPassedAllTheCheckPoints())
                    {
                        HVRAgent hvrAgent = col.GetComponent<HVRAgent>(); 
                        hvrAgent.RewardReachedGoal();
                        GetComponentsInChildren<HVRAgent>().ToList().ForEach(agent =>
                        {
                            agent.racer.IncrementLoopCount();
                            agent.AgentReset();
                        });
                    }
                    resetCheckpoints();
                }
            }

            hasCollidedWithStart = false;
        }
        else if (!hasCollidedWithStart && checkPointType == CheckPointType.Start)
        {
            col.GetComponent<RacerPosition>().hasCollidedWithStart = true;
        }
        else
        {
            col.GetComponent<RacerPosition>().hasCollidedWithStart = false;
        }
    }

    private bool hasPassedAllTheCheckPoints()
    {
        bool allCheckPointsPassed = true;
        GetComponent<RaceTrackCheckpointSpawner>().checkPointParentTrans.GetComponentsInChildren<CheckPointBehavior>()
            .ToList().ForEach(behavior =>
            {
                if (!behavior.hasDrivenThroughCheckpoint) allCheckPointsPassed = false;
            });
        return allCheckPointsPassed;
    }
    
    private void resetCheckpoints()
    {
        GetComponent<RaceTrackCheckpointSpawner>().GetComponentsInChildren<CheckPointBehavior>().ToList().ForEach(checkpointBehvaiour =>
            checkpointBehvaiour.hasDrivenThroughCheckpoint = false);
    }
}