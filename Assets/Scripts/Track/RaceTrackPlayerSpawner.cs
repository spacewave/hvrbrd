﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security.Cryptography;
using MLAgents;
using PathCreation;
using PathCreation.Examples;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;


[RequireComponent(typeof(RoadMeshCreatorExtend))]
public class RaceTrackPlayerSpawner : MonoBehaviour
{
    public static int vertCount = 2;

    public bool willSpawnPlayers;
    public GameObject hvrBoardPrefab;
    public GameObject hvrBoardPlayerPrefab;
    public GameObject hvrBoardPlayerSimulationPrefab;
    public bool spawnPlayerSimulation;

    private VertexPath _vertexPath;
    public VertexPath VertexPath => _vertexPath;

    private int _indexOfVerticesStart;
    public List<Racer> racers;

    public string indexOfStart;


    // Start is called before the first frame update
    void Start()
    {
        _indexOfVerticesStart = GetComponent<RaceTrackStartSpawner>().indexOfTrackEnd;
        _vertexPath = GetComponent<PathCreator>().path;
        
        if (RaceController.Instance != null)
        {
	    var racerToDisplay = RaceController.Instance.racerToDisplay;
	    RaceController.Instance.racers[racerToDisplay] = racers[racerToDisplay];
            racers.ForEach(racer => racer.LoopCount = 0);
	    SpawnRacers();
            StartCoroutine(waitBeforeSpawn());
        } else {
	    SpawnRacers();
	}


    }


    public void SetRacerSpawnPosition(GameObject racerGameObject)
    {
        Vector3 racerPosition = _vertexPath.vertices[((_indexOfVerticesStart + 1) % _vertexPath.vertices.Length)] + new Vector3(0, transform.position.y, 0) +
                                hvrBoardPrefab.transform.position;

        Quaternion racerRotation = Quaternion.LookRotation(
            _vertexPath.vertices[_indexOfVerticesStart] - _vertexPath.vertices[_indexOfVerticesStart + 1]);

        var position = racerPosition;
        racerGameObject.transform.rotation = racerRotation;

        position +=
            2 * racerGameObject.GetComponent<HVRAgent>().racer.startPosition.x * racerGameObject.transform.right -
            4 * racerGameObject.GetComponent<HVRAgent>().racer.startPosition.y * racerGameObject.transform.forward;
        racerGameObject.transform.position = position;
    }

    public void DespawnRacers()
    {
        List<HVRAgent> agents = GetComponentsInChildren<HVRAgent>().ToList();
        agents.ForEach(agent => Destroy(agent.gameObject));
    }

    public void SpawnRacers()
    {
        if (willSpawnPlayers)
        {
            Vector3 racerPosition = _vertexPath.vertices[((_indexOfVerticesStart + 1) % _vertexPath.vertices.Length)] + new Vector3(0, transform.position.y, 0) +
                                    hvrBoardPrefab.transform.position;

            Quaternion racerRotation = Quaternion.LookRotation(
                _vertexPath.vertices[_indexOfVerticesStart] - _vertexPath.vertices[_indexOfVerticesStart + 1]);


            racers.ForEach(racer =>
            {
                GameObject racerGameObject;

                switch (racer.racerType)
                {
                    case RacerType.Player:
                        if (spawnPlayerSimulation)
                        {
                            racerGameObject = Instantiate(
                                hvrBoardPlayerSimulationPrefab,
                                racerPosition,
                                racerRotation, transform);
                            racerGameObject.transform.position +=
                                2 * racer.startPosition.x * racerGameObject.transform.right -
                                4 * racer.startPosition.z * racerGameObject.transform.forward;
                            racerGameObject.transform.position =
                                racerGameObject.transform.position + new Vector3(0, racer.startPosition.y, 0);
                            racerGameObject.GetComponent<PlayerSimulationController>().racer = racer;
                            racerGameObject.GetComponent<PlayerSimulationController>().racer.name = PlayerPrefs.GetString("name");
                        }    
                        else
                        {
                            racerGameObject = Instantiate(
                                hvrBoardPlayerPrefab,
                                racerPosition,
                                racerRotation, transform);
                            racerGameObject.transform.position +=
                                2 * racer.startPosition.x * racerGameObject.transform.right -
                                4 * racer.startPosition.y * racerGameObject.transform.forward;
                            racerGameObject.GetComponent<PlayerController>().racer = racer;
                            racerGameObject.GetComponent<PlayerController>().racer.name = PlayerPrefs.GetString("name");
                            
                            Canvas canvas = GameObject.Find("Canvas").GetComponentInChildren<Canvas>();
                            canvas.renderMode = RenderMode.ScreenSpaceCamera;
                            canvas.worldCamera = racerGameObject.GetComponentInChildren<Camera>();
                        }


                        break;
                    case RacerType.AI:
                        racerGameObject = Instantiate(
                            hvrBoardPrefab,
                            racerPosition,
                            racerRotation, transform);

                        racerGameObject.transform.position +=
                            2 * racer.startPosition.x * racerGameObject.transform.right -
                            4 * racer.startPosition.y * racerGameObject.transform.forward;
                        racerGameObject.GetComponent<HVRAgent>().racer = racer;
                        break;
                    default:
                        racerGameObject = Instantiate(
                            hvrBoardPrefab,
                            racerPosition,
                            racerRotation, transform);

                        racerGameObject.transform.position +=
                            2 * racer.startPosition.x * racerGameObject.transform.right -
                            4 * racer.startPosition.y * racerGameObject.transform.forward;
                        racerGameObject.GetComponent<HVRAgent>().racer = racer;
                        break;
                }
            });
        }
    }
    
    IEnumerator waitBeforeSpawn()
    {
        Text textComponent = RaceController.Instance.canvas.transform.GetChild(2).GetComponent<Text>();
        float timeScale = Time.timeScale;
        Time.timeScale = 0f;
        textComponent.text = "3";
        yield return new WaitForSecondsRealtime(1);
        textComponent.text = "2";
        yield return new WaitForSecondsRealtime(1);
        textComponent.text = "1";
        yield return new WaitForSecondsRealtime(1);
        Time.timeScale = timeScale;
        textComponent.text = "GO";
        yield return new WaitForSecondsRealtime(0.5f);
        textComponent.text = "";
    }
}
