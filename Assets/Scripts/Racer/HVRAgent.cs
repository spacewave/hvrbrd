﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MLAgents;
#if UNITY_EDITOR
using UnityEditor.Experimental.UIElements.GraphView;
using UnityEngine.Experimental.UIElements;
#endif
using UnityEngine.Serialization;

[RequireComponent(typeof(RayPerception))]
public class HVRAgent : Agent
{
    public Racer racer;
    
    private RayPerception _rayPerception;
    public Checkpoint currentCheckPointGoal;
    public float rayView;
    public Movement movement;
    public float[] rayAngles;
    public string[] detectableObjects;

    public bool rewardGoal;
    public bool rewardCheckpoint;
    public bool rewardForwardDirection;
    public bool rewardDriveInMiddleOfCenter;
    public bool penaltyOnCrash;

    public override void InitializeAgent()
    {
        movement = GetComponent<Movement>();
        _rayPerception = GetComponent<RayPerception>();
    }

    public override void CollectObservations()
    {
        AddVectorObs(_rayPerception.Perceive(rayView, rayAngles, detectableObjects, 0, 0));
        AddVectorObs(GetComponent<Rigidbody>().velocity);
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        for (int index = 0; index < vectorAction.Length; index++)
            if (Math.Abs(vectorAction[index] - Single.Epsilon) < 0.1f)
            {
            }
            else
            {
                switch (index)
                {
                    case 0:
                        movement.Accelerate(Mathf.Clamp(vectorAction[index], -1f, 1f));
                        break;
                    case 1:
                        movement.Steer(Mathf.Clamp(vectorAction[index], -1f, 1f));
                        break;
                    case 2:
                        movement.Brake();
                        break;
                }
            }

        RewardForwardDirectionFunction();
        RewardDriveInMiddleOfCenterFunction();
        AddReward(-1f / agentParameters.maxStep);
    }

    public override void AgentReset()
    {
        racer.isFinished = false;
        racer.LoopCount = 0;
        gameObject.GetComponentInParent<RaceTrackPlayerSpawner>().SetRacerSpawnPosition(gameObject);
    }

    public override void AgentOnDone()
    {
        racer.isFinished = true;
    }

    private void RewardCheckPointsFunction()
    {
        if (rewardCheckpoint)
        {
            AddReward(0.5f);
        }
    }

    private void RewardForwardDirectionFunction()
    {
        if (rewardForwardDirection)
        {
            AddReward(Vector3.Dot(transform.forward, GetComponent<Rigidbody>().velocity) / 10000);
        }
    }

    private void PenaltyOnCrashFunction()
    {
        if (penaltyOnCrash)
        {
            AddReward(-2f);
        }
    }

    private void RewardDriveInMiddleOfCenterFunction()
    {
        if (rewardDriveInMiddleOfCenter)
        {
            Ray rayLeft = new Ray(transform.position, transform.right);
            Ray rayRight = new Ray(transform.position, -transform.right);

            if (Physics.Raycast(rayLeft, out RaycastHit raycastHitLeft) &&
                Physics.Raycast(rayRight, out RaycastHit raycastHitRight))
            {
                if (raycastHitLeft.collider.CompareTag("left_bounds") &&
                    raycastHitRight.collider.CompareTag("right_bounds"))
                {
                    AddReward((-Math.Abs(raycastHitLeft.distance - raycastHitRight.distance) + 5) / 100);
                }
            
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("right_bounds") || other.gameObject.CompareTag("left_bounds"))
        {
            PenaltyOnCrashFunction();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("checkpoint") &&
            other.gameObject.GetComponent<CheckPointBehavior>().checkPointType == CheckPointType.Orientation)
        {
            if (currentCheckPointGoal.id == other.gameObject.GetComponent<CheckPointBehavior>().checkpoint.id)
            {
                RewardCheckPointsFunction();
                currentCheckPointGoal = other.gameObject.GetComponent<CheckPointBehavior>().checkpoint.nextCheckPoint
                    .GetComponent<CheckPointBehavior>().checkpoint;
            }

            if (currentCheckPointGoal.id == -1)
            {
                if (other.gameObject.GetComponent<CheckPointBehavior>() != null && other.gameObject.GetComponent<CheckPointBehavior>().checkpoint.nextCheckPoint != null)
                {
                    currentCheckPointGoal = other.gameObject.GetComponent<CheckPointBehavior>().checkpoint
                        .nextCheckPoint
                        .GetComponent<CheckPointBehavior>().checkpoint;
                }
            }
        }
    }

    public void RewardReachedGoal()
    {
        if (rewardGoal)
        {
            AddReward(10f);
        }
    }
}
