﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class Racer
{
    public String name;
    public Vector3 startPosition;
    public RacerType racerType;
    public Color color;
    public bool isFinished;

    public Racer(bool isFinished)
    {
        this.isFinished = false;
    }


    private int _loopCount;

    public int LoopCount
    {
        get => _loopCount;
        set => _loopCount = value;
    }

    public void IncrementLoopCount()
    {
        _loopCount++;
    }
}

public enum RacerType
{
    Player,
    AI
}

