﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundController : MonoBehaviour
{
    public AudioSource accelerateAudio;
    public AudioSource crashAudio;
    private AudioSource _audioSourceAccelerate;
    private bool _highEdge = false;

    // Update is called once per frame
    void Update()
    {
        float y = Input.GetAxis("Vertical");
        if (y > 0.9f)
        {
            if (accelerateAudio != null)
            {
                if (!_highEdge)
                {
                    _highEdge = true;
                    accelerateAudio.Play();
                }

                accelerateAudio.volume = Mathf.Lerp(0, 1, 0.05f);
            }
        }

        if (y < 0.9f)
        {
            _highEdge = false;
            accelerateAudio.volume = Mathf.Lerp(accelerateAudio.volume, 0, 0.05f);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        crashAudio.Play();
    }
}