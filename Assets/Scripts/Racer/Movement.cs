﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Rigidbody rb;
    public float speed = 800;
    public float handling = 170;
    public float stabilize = 10;
    public float drift = 3;

    public float hoverHeight = 1;
    public float wallDistance = 14;

    public float hoverForce = 65;
    public float wallForce = 35;
    private Ray ray;
    private RaycastHit rayHit;
    private float proportionalHeight;
    private Vector3 appliedHoverForce;

    // Start is called before the first frame update
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision){
        rb.MovePosition(transform.position + new Vector3(0, 0.1f, 0));
    }
    // Update is called once per frame
    private void Update()
    {
        
    }

    void FixedUpdate()
    {
        bounceOff(-transform.up, Vector3.up, hoverHeight, hoverForce);
        bounceOff(-transform.right, transform.right, wallDistance, wallForce);
        bounceOff(transform.right, -transform.right, wallDistance, wallForce);
        bounceOff(transform.forward, -transform.forward, wallDistance, wallForce);

        Stabilize();
    }

    private void bounceOff(Vector3 direction, Vector3 pushDirection, float distance, float bounceForceFactor)
    {
        Ray ray = new Ray (transform.position, direction);
        
        if (Physics.Raycast(ray, out rayHit, wallDistance))
        {
            if(!rayHit.collider.tag.Equals("checkpoint") && !rayHit.collider.tag.Equals("board")){
                proportionalHeight = (distance - rayHit.distance) / wallDistance;
                appliedHoverForce = proportionalHeight * bounceForceFactor * pushDirection;

                rb.AddForce(appliedHoverForce, ForceMode.Acceleration);
            }
        }
    }

    public void Accelerate(float direction)
    {
        if (direction > 0 && Time.deltaTime > 0.01f)
        {
            rb.AddForce(direction * speed * transform.forward);
        }
    }

    public void Brake()
    {
        rb.AddForce(speed * (-transform.forward));
    }

    public void Steer(float radian)
    {
        rb.AddTorque(handling * radian * Vector3.up);
        rb.AddForce(drift * radian * transform.right);
    }

    private void Stabilize()
    {
        var rotation = transform.rotation;
        var nextRotation = Quaternion.Lerp(rotation, Quaternion.identity, 1 / stabilize);
        rotation = Quaternion.Euler(nextRotation.eulerAngles.x, rotation.eulerAngles.y, nextRotation.eulerAngles.z);
        transform.rotation = rotation;
    }
}
