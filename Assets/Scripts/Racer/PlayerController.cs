﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

[RequireComponent(typeof(Movement))]
public class PlayerController : MonoBehaviour
{
    private Movement _movement;
    public Racer racer;

    private float wb_x = 0;
    private float wb_y = 0;
    private float wb_total = 0;
    private float wb_tl = 0;
    private float wb_tr = 0;
    private float wb_bl = 0;
    private float wb_br = 0;

    private bool run = true;
    private UdpClient udpClient = new UdpClient();
    private IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
    private static string ip = "169.254.167.188";
    private static int port = 5666;
    private static Byte[] sendBytes = Encoding.ASCII.GetBytes("!");

    [Range(1f, 2f)]
    public float mappingX = 1.1f;
    [Range(1f, 2f)]
    public float mappingY = 1.5f;

    void udpSocketUpdate()
    {
        while (run)
        {
            try
            {
                Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                wb_tl = BitConverter.ToSingle(receiveBytes, 0);
                wb_tr = BitConverter.ToSingle(receiveBytes, 4);
                wb_bl = BitConverter.ToSingle(receiveBytes, 8);
                wb_br = BitConverter.ToSingle(receiveBytes, 12);
            }
            catch (Exception e)
            {
                wb_tl = 0;
                wb_tr = 0;
                wb_bl = 0;
                wb_br = 0;
                Debug.Log(e.ToString());
            }
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        _movement = GetComponent<Movement>();

        try
        {
            udpClient.Connect(ip, port);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
        Thread updateData = new Thread(udpSocketUpdate);
        updateData.Start();
    }

    // Update is called once per frame
    void Update()
    {
        udpClient.Send(sendBytes, sendBytes.Length);

        wb_total = wb_tl + wb_tr + wb_bl + wb_br;
        wb_x = ((wb_tr + wb_br) / wb_total) * 2 - 1;
        wb_y = ((wb_tl + wb_tr) / wb_total) * 2 - 1;

        //float x = Input.GetAxis("Horizontal");
        //float y = Input.GetAxis("Vertical");
        if (wb_total < 10)
        {
            _movement.Accelerate(0);
            _movement.Steer(0);
        } else
        {
            _movement.Accelerate(MapBoard(wb_y * mappingY));
            _movement.Steer(MapBoard(wb_x * mappingX));
        }
    }

    private float MapBoard(float value)
    {
        if (value > 1f)
        {
            return 1f;
        }
        else if (value  < -1f)
        {
            return -1f;
        }
        else
        {
            return value;
        }
    }

    void OnDisable()
    {
        run = false;
        udpClient.Close();
    }
}