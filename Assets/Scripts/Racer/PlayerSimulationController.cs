﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSimulationController : MonoBehaviour
{
    private Movement _movement;
    public Racer racer;

    // Start is called before the first frame update
    void Start()
    {
        _movement = GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        
        _movement.Accelerate(y);
        _movement.Steer(x);
    }
}