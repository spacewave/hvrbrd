#!/bin/bash
cd /sys/kernel/config/usb_gadget/

mkdir RNDIS
cd RNDIS

echo "0x0200" > bcdUSB
echo "0x02" > bDeviceClass
echo "0x00" > bDeviceSubClass
echo "0x3066" > bcdDevice
echo "0x1d6b" > idVendor
echo "0x0105" > idProduct

echo "1" > os_desc/use
echo "0xcd" > os_desc/b_vendor_code
echo "MSFT100" > os_desc/qw_sign

mkdir strings/0x409
echo "000047ecb54f" > strings/0x409/serialnumber
echo "RaspberryPi" > strings/0x409/manufacturer
echo "HVRBRD" > strings/0x409/product

mkdir configs/c.1
mkdir configs/c.1/strings/0x409
echo "FunctionFS RNDIS" > configs/c.1/strings/0x409/configuration

mkdir functions/rndis.usb0

echo "RNDIS" > functions/rndis.usb0/os_desc/interface.rndis/compatible_id
echo "5162001" > functions/rndis.usb0/os_desc/interface.rndis/sub_compatible_id

echo "12:00:47:ec:b5:4f" > functions/rndis.usb0/host_addr
echo "02:00:47:ec:b5:4f" > functions/rndis.usb0/dev_addr

ln -s functions/rndis.usb0 configs/c.1

ln -s configs/c.1 os_desc

echo "20980000.usb" > UDC