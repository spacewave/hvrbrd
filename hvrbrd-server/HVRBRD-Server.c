#include <stdio.h>
#include <pthread.h>
#include <wiiuse.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>

#define MAX_WIIMOTES 1
#define FIND_TIMEOUT 5
#define PORT 5666
#define BUFLEN 8

pthread_mutex_t mutex;
char run = 1;
char wiiUseRdy = 0;
float globalBoardArray[4] = {0};

short any_wiimote_connected(wiimote **wm, int wiimotes)
{
    int i;
    if (!wm)
    {
        return 0;
    }

    for (i = 0; i < wiimotes; i++)
    {
        if (wm[i] && WIIMOTE_IS_CONNECTED(wm[i]))
        {
            return 1;
        }
    }

    return 0;
}

void handle_event(struct wiimote_t *wm, float boardArray[])
{
    if (wm->exp.type == EXP_WII_BOARD)
    {
        struct wii_board_t *wb = (wii_board_t *)&wm->exp.wb;
        boardArray[0] = wb->tl;
        boardArray[1] = wb->tr;
        boardArray[2] = wb->bl;
        boardArray[3] = wb->br;
    }
}

void handle_ctrl_status(struct wiimote_t *wm)
{
    printf("CONTROLLER STATUS [wiimote id %i]\n", wm->unid);
    printf("battery: %f %%\n", wm->battery_level);
}

void handle_disconnect(wiimote *wm)
{
    printf("DISCONNECTED [wiimote id %i]\n", wm->unid);
}

void *udpSocket(void *arg)
{
    int udpSocket;
	int sockOpt;
    struct sockaddr_in servaddr, cliaddr;
    socklen_t cliaddr_len = sizeof(cliaddr);
    char buffer[BUFLEN];
	struct timeval tv;
	
	tv.tv_sec = 0;
	tv.tv_usec = 500000;

    udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (udpSocket == -1)
    {
        printf("Failed to created UDP socket.\n");
        pthread_mutex_lock(&mutex);
        run = 0;
        pthread_mutex_unlock(&mutex);
        return 0;
    }
    else
    {
        printf("Created UDP socket.\n");
    }

	sockOpt = setsockopt(udpSocket, SOL_SOCKET, SO_RCVTIMEO, &tv,sizeof(tv));

	if (sockOpt == -1)
    {
        printf("Failed to set socket options.\n");
        pthread_mutex_lock(&mutex);
        run = 0;
        pthread_mutex_unlock(&mutex);
        return 0;
    }
    else
    {
        printf("Socket options set.\n");
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    if (bind(udpSocket, (const struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
    {
        printf("Failed to bind socket.\n");
        pthread_mutex_lock(&mutex);
        run = 0;
        pthread_mutex_unlock(&mutex);
        return 0;
    }
    else
    {
        printf("Socket listen on port %i.\n", PORT);
    }

    while (run)
    {
        if (wiiUseRdy)
        {
            recvfrom(udpSocket, (char *)buffer, BUFLEN, 0, (struct sockaddr *)&cliaddr, &cliaddr_len);
            sendto(udpSocket, (const char *)globalBoardArray, sizeof(globalBoardArray), 0, (const struct sockaddr *)&cliaddr, cliaddr_len);
        }
    }

    close(udpSocket);

    return 0;
}

void *wbWiiUse(void *arg)
{
    wiimote **wiimotes;
    int found, connected;
    float boardArray[4] = {0};

    wiimotes = wiiuse_init(MAX_WIIMOTES);

    printf("\nPress the red sync button on the board now - searching for %i seconds.\n\n", FIND_TIMEOUT);

    found = wiiuse_find(wiimotes, MAX_WIIMOTES, FIND_TIMEOUT);

    if (!found)
    {
        printf("No wiimotes found.\n");
        pthread_mutex_lock(&mutex);
        run = 0;
        pthread_mutex_unlock(&mutex);
        return 0;
    }

    connected = wiiuse_connect(wiimotes, MAX_WIIMOTES);
    if (connected)
    {
        printf("Connected to %i wiimotes (of %i found).\n", connected, found);
    }
    else
    {
        printf("Failed to connect to any wiimote.\n");
        pthread_mutex_lock(&mutex);
        run = 0;
        pthread_mutex_unlock(&mutex);
        return 0;
    }

	wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1);

    pthread_mutex_lock(&mutex);
    wiiUseRdy = 1;
    pthread_mutex_unlock(&mutex);

    while ((any_wiimote_connected(wiimotes, MAX_WIIMOTES)) && run)
    {
        int events;
        do
        {
            events = wiiuse_poll(wiimotes, MAX_WIIMOTES);

            if (events)
            {
                int i = 0;
                for (; i < MAX_WIIMOTES; ++i)
                {
                    switch (wiimotes[i]->event)
                    {
                    case WIIUSE_EVENT:
                        handle_event(wiimotes[i], boardArray);
                        pthread_mutex_lock(&mutex);
                        memcpy(globalBoardArray, boardArray, 4 * sizeof(float));
                        pthread_mutex_unlock(&mutex);
                        break;

                    case WIIUSE_STATUS:
                        handle_ctrl_status(wiimotes[i]);
                        break;

                    case WIIUSE_DISCONNECT:
                    case WIIUSE_UNEXPECTED_DISCONNECT:
                        handle_disconnect(wiimotes[i]);
                        break;
                    default:
                        break;
                    }
                }
            }
        } while (!events);
    }

    wiiuse_cleanup(wiimotes, MAX_WIIMOTES);

    pthread_mutex_lock(&mutex);
    run = 0;
    pthread_mutex_unlock(&mutex);

    return 0;
}

int main()
{
    pthread_t udpSocketThread, wiiUseThread;

    printf("HVRBRD - Wii Balance Board - Peripherie Testserver\n\n");

    pthread_mutex_init(&mutex, NULL);

    pthread_create(&udpSocketThread, NULL, udpSocket, NULL);
    pthread_create(&wiiUseThread, NULL, wbWiiUse, NULL);

    pthread_join(udpSocketThread, NULL);
    pthread_join(wiiUseThread, NULL);

    return 0;
}
